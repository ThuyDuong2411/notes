package com.d.diary.model;

import android.graphics.Color;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class JournalItem implements Serializable {
    private String id;
    private Date date;
    private Date alarm;
    private String title;
    private String content;
    private int color;
    private int alert;
    private List<HistoryItem> historyList;

    public JournalItem() {
        historyList = new ArrayList<>();
    }

    public JournalItem(Date date, String title, String content, int color, Date alarm, int alert) {
        this.date = date;
        this.title = title;
        this.content = content;
        this.color = color;
        this.alarm = alarm;
        this.alert = alert;
        historyList = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) { this.color = color; }

    public Date getAlarm() { return alarm; }

    public void setAlertDate(Date alarm) { this.alarm = alarm; }

    public int getAlert() { return alert; }

    public void setAlert(int alarm) { alert = alarm; }

    public List<HistoryItem> getHistoryList() {  return historyList; }

    public void setHistoryList(List<HistoryItem> historyList) { this.historyList = historyList; }
}
