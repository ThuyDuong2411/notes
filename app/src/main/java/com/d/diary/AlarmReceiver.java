package com.d.diary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("alarm", "hhhh");
        String music = intent.getExtras().getString("extra");
        Log.e("key",music);
        Intent myIntent = new Intent(context, Music.class);
        myIntent.putExtra("extra", music);
        context.startService(myIntent);
    }
}
